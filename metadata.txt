# This file contains metadata for your plugin. Since 
# version 2.0 of QGIS this is the proper way to supply 
# information about a plugin. The old method of 
# embedding metadata in __init__.py will 
# is no longer supported since version 2.0.

# This file should be included when you package your plugin.# Mandatory items:

[general]
name=CIGeoE Converge Lines
qgisMinimumVersion=3.0
description=Creates a point of convergence of lines within a selected area.
version=1.0.0
author=Centro de Informação Geoespacial do Exército
email=igeoe@igeoe.pt

about=Plugin to create a point of convergence of lines within a selected area. This tool searches for line endpoints inside the selected area, calculates the midpoint point between those vertices and translate the endpoints to the average point coordinates.

tracker=https://gitlab.com/cigeoe/cigeoe_converge_lines/-/issues
repository=https://gitlab.com/cigeoe/cigeoe_converge_lines
# End of mandatory metadata

# Recommended items:

# Uncomment the following line and add your changelog:
# changelog=

# Tags are comma separated with spaces allowed
tags=3d,vector,layers

homepage=https://gitlab.com/cigeoe/cigeoe_converge_lines
category=Plugins
icon=icon.png
# experimental flag
experimental=False

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False