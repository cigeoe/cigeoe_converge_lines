Converge Lines is a plugin for QGis.

Plugin to create a point of convergence of lines within a selected area.

This tool searches for line endpoints inside the selected area, calculates the midpoint point between those vertices and translate the endpoints to the average point coordinates.

Note: any line with the start and end points inside the selected area will be removed.


Usage:
- activate the plugin and set the target layer (linestring type) as editable;
![ALT](/images/image1.jpg)
- draw a rectangle by pressing (left button) and dragging the mouse over the line endpoints that you wish to converge in a midpoint point;
![ALT](/images/image2.jpg)
- after releasing the mouse button, the line endpoints will be translated to the midpoint coordinates.
![ALT](/images/image3.jpg)


Centro de Informação Geoespacial do Exército (www.igeoe.pt)

Portugal
