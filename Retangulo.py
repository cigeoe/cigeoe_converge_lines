# -*- coding: utf-8 -*-

#from PyQt5.QtCore import *
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QMessageBox


from qgis.gui import QgsMessageBar, QgsMapToolEmitPoint, QgsRubberBand
#from qgis.gui import *
from qgis.core import QgsWkbTypes, Qgis, QgsPointXY, QgsRectangle, QgsMapLayer, QgsFeatureRequest
#from qgis.core import *
from qgis.utils import iface 

class Retangulo(QgsMapToolEmitPoint): 
           
    def __init__(self, canvas, iface):
        """
        draws a rectangle, then features that intersect this rectangle are added to selection 
        """
        # Store reference to the map canvas
        self.canvas = canvas  

        self.iface = iface        # add iface for work with active layer                
                
        QgsMapToolEmitPoint.__init__(self, self.canvas)
        
        #self.rubberBand = QgsRubberBand(self.canvas, geometryType=QGis.Line)              # pyqgis2
        self.rubberBand = QgsRubberBand(self.canvas, geometryType=QgsWkbTypes.LineGeometry)
        self.rubberBand.setColor(QColor(0, 0, 240, 100))
        self.rubberBand.setWidth(1)
        
        self.reset() 
               

    def reset(self):
        self.startPoint = self.endPoint = None
        self.isEmittingPoint = False
        #self.rubberBand.reset( QGis.Polygon )                 # pyqgis2
        self.rubberBand.reset( QgsWkbTypes.PolygonGeometry )


    def canvasPressEvent(self, e):
        """
        Method used to build rectangle if shift is held, otherwise, feature select/deselect and identify is done.
        """
        self.startPoint = self.toMapCoordinates(e.pos())
        self.endPoint = self.startPoint
        self.isEmittingPoint = True
        
        self.showRect(self.startPoint, self.endPoint)
        
    #self.isEmittingPoint is used to build the rubberband on the method on CanvasMoveEvent

            
    def canvasMoveEvent(self, e):
        """
        Used only on rectangle select.
        """
        if not self.isEmittingPoint:
            return
        self.endPoint = self.toMapCoordinates( e.pos() )
        self.showRect(self.startPoint, self.endPoint)

   
    def showRect(self, startPoint, endPoint):
        """
        Builds rubberband rect.
        """
        #self.rubberBand.reset(QGis.Polygon)                # pyqgis2 
        self.rubberBand.reset( QgsWkbTypes.PolygonGeometry )
        if startPoint.x() == endPoint.x() or startPoint.y() == endPoint.y():
            return

        #point1 = QgsPoint(startPoint.x(), startPoint.y())       # pyqgis2
        #point2 = QgsPoint(startPoint.x(), endPoint.y())         # pyqgis2
        #point3 = QgsPoint(endPoint.x(), endPoint.y())           # pyqgis2
        #point4 = QgsPoint(endPoint.x(), startPoint.y())         # pyqgis2
        point1 = QgsPointXY(startPoint.x(), startPoint.y())
        point2 = QgsPointXY(startPoint.x(), endPoint.y())
        point3 = QgsPointXY(endPoint.x(), endPoint.y())
        point4 = QgsPointXY(endPoint.x(), startPoint.y())

        self.rubberBand.addPoint(point1, False)
        self.rubberBand.addPoint(point2, False)
        self.rubberBand.addPoint(point3, False)
        self.rubberBand.addPoint(point4, True)    # true to update canvas
        self.rubberBand.show()


    def rectangle(self):
        """
        Builds rectangle from self.startPoint and self.endPoint
        """
        if self.startPoint is None or self.endPoint is None:
            return None
        elif self.startPoint.x() == self.endPoint.x() or self.startPoint.y() == self.endPoint.y():
            return None
        return QgsRectangle(self.startPoint, self.endPoint)

    
    def canvasReleaseEvent(self, e):
        """
        After the rectangle is built, here features are selected.
        """
        self.isEmittingPoint = False



        r = self.rectangle()            #QgsRectangle

        
        if r is not None:
            
            layer = self.iface.activeLayer()   # work with active layer  
           
            #rectSelLayers=[]      
            rectSelFeats=[]
            vDentro = []           
            coordx = 0.0
            coordy = 0.0

            #*******************************************************************
            # iterate with all layers
            #*******************************************************************
            #layers = self.canvas.layers()
            #for layer in layers:
                #ignore => layers != VectorLayer and geometry != line 
                #if layer.type() != QgsMapLayer.VectorLayer or layer.geometryType() != QGis.Line:  
                    #continue

                #builds bbRect and select from layer, adding selection
                #bbRect = self.canvas.mapSettings().mapToLayerCoordinates(layer, r) 
               
                #for f in layer.getFeatures():  

                    #if f.geometry().intersects(bbRect):    
                        #rectSelFeats.append( f ) 
                        #rectSelLayers.append(layer)
                        
            #self.rubberBand.hide()
            #*******************************************************************

            bbRect = self.canvas.mapSettings().mapToLayerCoordinates(layer, r) 
            for f in layer.getFeatures(QgsFeatureRequest(bbRect)): 
                #if f.geometry().intersects(bbRect):    
                rectSelFeats.append( f ) 
                        
            self.rubberBand.hide()

            n=0     # calc midpoint 
            if(len(rectSelFeats) !=0): 
                                                
                for linha in rectSelFeats:
                                         
                    verticesArray = linha.geometry().asPolyline()      
                    primeiroVertice = verticesArray[0]
                    ultimoVertice = verticesArray[len(verticesArray)-1]       
                    fora = 1
                    #  points within the rectangle
                    if (r.xMinimum() <= primeiroVertice.x() <= r.xMaximum()) and (r.yMinimum() <= primeiroVertice.y() <= r.yMaximum()) :
                        if (r.xMinimum() <= ultimoVertice.x() <= r.xMaximum()) and (r.yMinimum() <= ultimoVertice.y() <= r.yMaximum()) :
                            for point in verticesArray:   
                                if bbRect.contains(point) == False: 
                                    fora = 2 
                                    break

                            if fora == 1:
                                vDentro.append(0)         #  0=delete line within the rectangle
                            else:                                                        
                                vDentro.append(4)         #  4=changes first and last Vertex
                                coordx = coordx + primeiroVertice.x()
                                coordy = coordy + primeiroVertice.y()
                                n+=1                                          
                                coordx = coordx + ultimoVertice.x()
                                coordy = coordy + ultimoVertice.y()
                                n+=1  
                        else:
                            vDentro.append(1)         #  1=changes first Vertex
                            coordx = coordx + primeiroVertice.x()
                            coordy = coordy + primeiroVertice.y()
                            n+=1
                    else:                        
                        if (r.xMinimum() <= ultimoVertice.x() <= r.xMaximum()) and (r.yMinimum() <= ultimoVertice.y() <= r.yMaximum()) :
                            vDentro.append(2)         #  2=changes last Vertex              
                            coordx = coordx + ultimoVertice.x()
                            coordy = coordy + ultimoVertice.y()
                            n+=1
                        else:
                            vDentro.append(3)         #  3=without alteration
                          
                # calc midpoint
                if n > 1:
                    coordx = coordx / n   
                    coordx = round(coordx, 3)                
                    
                    coordy = coordy / n 
                    coordy = round(coordy, 3)  

                    pontoMedio = (coordx,coordy)

                   
            #change first/last vertex or delete line
             
            #if((len(vDentro) !=0) and n > 1 ):  
            if(len(vDentro) !=0) :  

                prov = layer.dataProvider()                 
                execflag = 0   # to control the messageBar
                toDeleteIdx=[]

                for idx, val in enumerate(rectSelFeats):

                    if vDentro[idx] != 3:                              
                        #prov = rectSelLayers[idx].dataProvider() 
                        feat = rectSelFeats[idx]
                        geom = feat.geometry()

                        if vDentro[idx] == 0:          # store line id to delete later
                            toDeleteIdx.append(feat.id())
                            #rectSelLayers[idx].triggerRepaint()        
                            execflag = 1                        

                        if vDentro[idx] == 1:          # first vertex: inserts new and delete initial vertex
                            geom.moveVertex(coordx, coordy, 0 ) 
                            prov.changeGeometryValues({feat.id(): geom})  
                            #rectSelLayers[idx].triggerRepaint()        
                            execflag = 1
                                                                                                                        
                        if vDentro[idx] == 2:          # last vertex: inserts new and delete last vertex
                            verticesArray = rectSelFeats[idx].geometry().asPolyline()      
                            geom.moveVertex(coordx, coordy, (len(verticesArray)-1) ) 
                            prov.changeGeometryValues({feat.id(): geom})   
                            #rectSelLayers[idx].triggerRepaint()        
                            execflag = 1  

                        if vDentro[idx] == 4:          # first and last vertex: inserts new first and last vertex
                            verticesArray = rectSelFeats[idx].geometry().asPolyline()
                            geom.moveVertex(coordx, coordy, 0 )      
                            geom.moveVertex(coordx, coordy, (len(verticesArray)-1) )  
                            prov.changeGeometryValues({feat.id(): geom})  
                            #rectSelLayers[idx].triggerRepaint()        
                            execflag = 1
                                                                                                                      
                        
                #delete lines
                prov.deleteFeatures(toDeleteIdx)
                              
                if execflag == 1: 
                    layer.triggerRepaint() 
                    
                    #self.iface.messageBar().pushMessage("CIGeoE Converge Lines", "Convergence done" , level=QgsMessageBar.INFO, duration=1) # pyqgis2
                    self.iface.messageBar().pushMessage("CIGeoE Converge Lines", "Convergence done" , level=Qgis.Info, duration=2)
            
            

